﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ProductDrag : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    private Camera mainCamera;
    private Vector2 startPosition;

    private int productId;
    // Polygon과 ProductDrag의 관계 재고
    private Polygon polygon;

    private void Start()
    {
        mainCamera = GameObject.Find("MainCamera").GetComponent<Camera>();
    }

    public void InitProductDrag(Polygon paramPolygon)
    {
        polygon = paramPolygon;
    }

    public void SetPorduct(int id)
    {
        productId = id;
        startPosition = transform.localPosition;
        //Debug.Log("Set Prodcut id : " + productId);
        //Debug.Log("Set Prodcut name : " + polygon.name);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        //Debug.Log("Begined drag product id : " + productId);
        //Debug.Log("Begined drag name : " + polygon.name);
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector3 currentPos = eventData.position;
        currentPos = mainCamera.ScreenToWorldPoint(currentPos);
        currentPos.z = transform.position.z;
        transform.position = currentPos;
    }

    public void OnEndDrag(PointerEventData eventData)
    {

        if(FieldManager.Instance.JudgePlacePolygon(mainCamera.ScreenToWorldPoint(eventData.position), polygon))
        {
            ProductManager.Instance.ReleasePolygon(productId);
        } else
        {
            transform.localPosition = startPosition;
        }
        //Debug.Log("Draged product id : " + productId);
        //Debug.Log("Draged product name : " + polygon.name);
    }
}
