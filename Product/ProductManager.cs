﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductManager : SingletonMonoBehaviour<ProductManager>
{
    public class ProductBox
    {
        public GameObject gameObject;
        public Vector3 position;
        public Polygon polygon;
    }

    private const int MAX_PRODUCT_NUM = 3;
    private const string NAME_PRODUCT_BOX = "ProductBox";
    private const string NAME_PRODUCT = "Product";
    private int[] availablePolygons;
    private ProductBox[] productBoxList;

    private void Start()
    {
        InitializeProductManager();
    }

    private void InitializeProductManager()
    {
        availablePolygons = BattleManager.Instance.currentAvailablePolygons;
        productBoxList = new ProductBox[3];

        for (int i = 0; i < MAX_PRODUCT_NUM; i++)
        {
            productBoxList[i] = new ProductBox();
            productBoxList[i].gameObject = transform.Find(NAME_PRODUCT_BOX + (i + 1).ToString()).gameObject;
            productBoxList[i].position = productBoxList[i].gameObject.GetComponent<Transform>().position;
            productBoxList[i].polygon = null;
        }
    }

    public void CreatePolygon()
    {
        // Inspect for any vacancies
        bool isVacancy = false;
        int choiceProductNum = 0;
        for (int i = 0; i < productBoxList.Length; i++)
        {
            if (productBoxList[i].polygon == null)
            {
                choiceProductNum = i;
                isVacancy = true;
                break;
            }
        }
        if (!isVacancy)
        {
            FailedCreatePolygon();
            return;
        }
        // Initialize seed
        Random.InitState((int)(Time.time * 100));

        // Initialze new product of polygon)
        int choiceAvailablePolygonID = Random.Range(0, Common.MAX_AVAILABLE_POLYGON_NUM);
        Polygon choicePolygon = PolygonManager.Instance.CreatePolygon((availablePolygons[choiceAvailablePolygonID]));
        choicePolygon.transform.position = productBoxList[choiceProductNum].position;
        choicePolygon.productDrag.SetPorduct(choiceProductNum);

        productBoxList[choiceProductNum].polygon = choicePolygon;
        //Debug.Log("Create product id : " + choiceProductNum);
        //Debug.Log("Create product name : " + choicePolygon.gameObject.name);
    }

    public void ReleasePolygon(int productNum)
    {
        PolygonManager.Instance.AddActivePolygon(productBoxList[productNum].polygon);
        productBoxList[productNum].polygon = null;
    }

    public void FailedCreatePolygon()
    {

    }
}
