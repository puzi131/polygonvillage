﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveData
{
    public int enemyID;
    public int startPositionX;
    public int startPositionY;
    public float birthDelay;
}
