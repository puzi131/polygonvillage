﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyManager : SingletonMonoBehaviour<EnemyManager>
{
    private const float SPEED_OFFSET = 0.005f;
    private int currentEnemyNo;
    private List<Enemy> activeEnemyList = new List<Enemy>();

    private void Update()
    {
        for (int i = 0; i < activeEnemyList.Count; i++)
        {
            activeEnemyList[i].UpdateEnemy();
        }
    }

    public Enemy CreatEnemy(int id)
    {
        Enemy enemy = new Enemy();
        var enemyData = Database.enemy.sheets[Common.BASE_SHEET].list.FirstOrDefault(param => param.id == id);
        enemy.enemyData.id = id;
        enemy.enemyData.enemyNo = currentEnemyNo++;
        enemy.enemyData.speed = enemyData.speed * SPEED_OFFSET;
        enemy.enemyData.fixedSpeed = enemyData.speed * SPEED_OFFSET;
        enemy.enemyData.hp = enemyData.hp;
        enemy.prefabName = enemyData.prefab;
        activeEnemyList.Add(enemy);
        return enemy;
    }

    public void SetEnemyPath()
    {
        for (int i = 0; i < activeEnemyList.Count; i++)
        {
            activeEnemyList[i].SetPath();
        }
    }

    public void RemoveEnemy(Enemy enemy)
    {
        enemy.EndAbnormality();
        enemy.gameObject.SetActive(false);
        activeEnemyList.Remove(enemy);
    }

    public List<Enemy> GetActiveEnemyList()
    {
        return activeEnemyList;
    }

    public bool IsEmptyEnemy()
    {
        return activeEnemyList.Count == 0;
    }
}
