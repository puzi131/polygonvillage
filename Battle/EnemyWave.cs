﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyWave
{
    private List<WaveData> waveDatas = new List<WaveData>();
    private int currentWaveNo;
    private int currentEnemyNo;
    private bool isFinalEnemy;
    private Timer timer = new Timer();

    private enum UpdateType
    {
        Battle,
        Pause,
    }

    private UpdateType currentUpdateType;

    public void InitEnemyWave()
    {
        currentUpdateType = UpdateType.Battle;
        timer.ResetTimer();
        currentEnemyNo = 0;
    }
    public void SetCurrentWave(int waveNo)
    {
        currentWaveNo = waveNo;
        isFinalEnemy = false;
    }

    public void SetWaveEnemy()
    {
        Debug.Log("Start wave" + currentWaveNo);
        //현재 웨이브 데이터 로드
        List<Entity_EnemyWave.Param> currentWaveList = Database.enemyWave.sheets[Common.BASE_SHEET].list.FindAll(x => x.wave == currentWaveNo);

        for (int i = 0; i < currentWaveList.Count; i++)
        {
            WaveData waveData = new WaveData();
            waveData.enemyID = currentWaveList[i].enemyID;
            waveData.birthDelay = currentWaveList[i].birthDelay;
            waveData.startPositionX = currentWaveList[i].startPositionX;
            waveData.startPositionY = currentWaveList[i].startPositionY;
            waveDatas.Add(waveData);
        }

        isFinalEnemy = false;
        timer.ResetTimer();
    }

    private void ManageSortie()
    {
        if (isFinalEnemy)
            return;

        if (timer.GetTime() > waveDatas[currentEnemyNo].birthDelay)
        {
            Debug.Log("Enemy number" + currentEnemyNo);
            SortieEnemy();
            timer.ResetTimer();
            if (waveDatas.Count - 1 == currentEnemyNo)
                isFinalEnemy = true;
            else
                NextEnemy();
        }
    }

    private void NextEnemy()
    {
        currentEnemyNo++;
    }
    private void SortieEnemy()
    {
        Enemy enemy = EnemyManager.Instance.CreatEnemy(waveDatas[currentEnemyNo].enemyID);
        enemy.InitEnemy();
        enemy.SetStartPosition(waveDatas[currentEnemyNo].startPositionX, waveDatas[currentEnemyNo].startPositionY);
        enemy.SetPath();
        enemy.SetPrefab();
        enemy.Departure();
    }

    private bool CheckClearWave()
    {
        if (isFinalEnemy && EnemyManager.Instance.IsEmptyEnemy())
        {
            return true;
        }
        return false;
    }

    // 웨이브 데이터 수정 필요
    private bool CheckFinalWave()
    {
        return false;
    }

    public void UpdateWave()
    {
        switch (currentUpdateType)
        {
            case UpdateType.Battle:
                timer.UpdateTimer();
                if (CheckClearWave())
                {
                    NextWave();
                }
                ManageSortie();
                break;

            case UpdateType.Pause:

                break;
        }
    } 

    public void EndBattle()
    {
        Debug.Log("End battle");
        currentUpdateType = UpdateType.Pause;
    }

    public void EndWave()
    {
        Debug.Log("End wave");
    }

    // 웨이브 데이터 수정 필요
    public void NextWave()
    {
        NextEnemy();
        currentWaveNo++;
        if (Database.enemyWave.sheets[Common.BASE_SHEET].list.FirstOrDefault(x => x.wave == currentWaveNo) == null)
        {
            // 마지막 웨이브 지남
            EndBattle();
        }
        else
            SetWaveEnemy();
    }
}
