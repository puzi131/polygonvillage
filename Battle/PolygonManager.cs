﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolygonManager : SingletonMonoBehaviour<PolygonManager>
{
    private List<Polygon> activePolygons = new List<Polygon>();

    private void Update()
    {
        for (int i = 0; i < activePolygons.Count; i++)
        {
            activePolygons[i].Update();
        }
    }

    public void AddActivePolygon(Polygon polygon)
    {
        activePolygons.Add(polygon);
    }

    public Polygon CreatePolygon(int polygonId)
    {
        Polygon polygon = new Polygon();
        InitializePolygon(polygon, polygonId);
        return polygon;
    }

    private void InitializePolygon(Polygon polygon, int polygonId)
    {
        polygon.InitPolygon(polygonId);
        polygon.SetPrefab();
    }
}
