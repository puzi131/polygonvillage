﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BattleManager : SingletonMonoBehaviour<BattleManager>
{
    private const string PATH_THREE_POLYGON_OBJECT = "prefabs/Three";
    private const string PATH_THREE_POLYGON_IMAGE = "Images/three";
    private const int START_WAVE_NUM = 1;

    public int[] currentAvailablePolygons = new int[Common.MAX_AVAILABLE_POLYGON_NUM];
    public List<Enemy> currentEnemyList = new List<Enemy>();
    public EnemyWave enemyWave = new EnemyWave();


    private void Start()
    {
        InitTempCurrentAvailablePolygons();
        InitBattleManager();
        //StartCoroutine(MousePointer());
    }

    private void Update()
    {
        UpdateBattlePhase();
    }

    private void UpdateBattlePhase()
    {
        enemyWave.UpdateWave();
    }

    public void InitBattleManager()
    {
        enemyWave.SetCurrentWave(START_WAVE_NUM);
        enemyWave.SetWaveEnemy();
    }
    
    /// <summary>
    /// 임시
    /// </summary>
    public void InitTempCurrentAvailablePolygons()
    {
        for (int i = 0; i < 5; i++)
        {
            currentAvailablePolygons[i] = i+1;
        }
    }

    private IEnumerator MousePointer()
    {
        while(true)
        {
            var mousePosition = Input.mousePosition;
            Debug.Log("MousePosition : " + mousePosition);
            yield return new WaitForSeconds(0.3f);
        }
    }
}
