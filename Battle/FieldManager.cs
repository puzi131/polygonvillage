﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AstarNode = AStarAlgorithm.AstarNode;
public class FieldManager : SingletonMonoBehaviour<FieldManager>
{
    private const float FIELD_POSITION_DIFFERENCE = 1.6f;
    private const int FIELD_ENEMY_POSITION_X = 0;
    private const int FIELD_ENEMY_POSITION_Y = 6;
    private const int FIELD_POLYGON_START_Y = 1;
    private const int FIELD_POLYGON_END_Y = 6;
    private static Vector2 FIELD_POSITION_OFFSET = new Vector2(-3.2f, -6.9f);
    private static Vector2 FIELD_MIN_POSITION = new Vector2(0, 0);
    private static Vector2 FIELD_MAX_POSITION = new Vector2(5f, 5f);
    private static Vector2[,] fieldEachPosition = new Vector2[5, 7];
    public Polygon[,] fieldPolygons = new Polygon[5, 7];

    private void Start()
    {
        Initialized();
    }

    private void Initialized()
    {
        for (int i = 0; i < fieldEachPosition.GetLength(0); i++)
        {
            for (int j = 0; j < fieldEachPosition.GetLength(1); j++)
            {
                fieldEachPosition[i, j] = new Vector2(i * FIELD_POSITION_DIFFERENCE + FIELD_POSITION_OFFSET.x, j * FIELD_POSITION_DIFFERENCE + FIELD_POSITION_OFFSET.y);                
            }
        }
    }

    public void PlacePolygon(int x, int y, Polygon polygon)
    {
        fieldPolygons[x, y] = polygon;
        polygon.productDrag.enabled = false;
        polygon.transform.localPosition = fieldEachPosition[x, y];

        // Init skill
        polygon.skill.SetBasePosition(polygon.transform.position);

        EnemyManager.Instance.SetEnemyPath();
    }

    /// <summary>
    /// 배치된 도형의 위치를 판단
    /// </summary>
    /// <param name="position"></param>
    /// <param name="polygon"></param>
    public bool JudgePlacePolygon(Vector2 position, Polygon polygon)
    {
        for (int i = 0; i < fieldEachPosition.GetLength(0); i++)
        {
            for (int j = FIELD_POLYGON_START_Y; j < FIELD_POLYGON_END_Y; j++)
            {
                if (Vector2.Distance(fieldEachPosition[i, j], position) < FIELD_POSITION_DIFFERENCE / 2)
                {
                    // 비어있으면 생성
                    if (fieldPolygons[i, j] == null)
                    {
                        AStarAlgorithm aStarAlgorithm = new AStarAlgorithm();
                        aStarAlgorithm.SetFieldInfo(CreateObstacle(i, j));
                        List<AstarNode> pathList = aStarAlgorithm.FindAStar(FIELD_ENEMY_POSITION_X, FIELD_ENEMY_POSITION_Y);
                        // 알맞은 길이 없음
                        if (pathList == null)
                            return false;
                        //LogPath(pathList);
                        PlacePolygon(i, j, polygon);
                        return true;
                    }
                    // 비어있지 않으면 취소
                    else
                    {
                        return false;
                    }
                }
            }
        }
        return false;
    }

    /// <summary>
    /// 기존 장애물 정보 + 새로운 장애물정보(파라미터)
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public bool[,] CreateObstacle(int x, int y)
    {
        bool[,] obstacleInfo = CreateObstacle();
        obstacleInfo[x, y] = true;
        return obstacleInfo;
    }

    public bool[,] CreateObstacle()
    {
        bool[,] obstacleInfo = new bool[5, 7];
        for (int i = 0; i < obstacleInfo.GetLength(0); i++)
        {
            for (int j = 0; j < obstacleInfo.GetLength(1); j++)
            {
                obstacleInfo[i, j] = fieldPolygons[i, j] != null ? true : false;
            }
        }
        return obstacleInfo;
    }

    /// <summary>
    /// 미완성
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    private void RemovePolygon(int x, int y)
    {
        fieldPolygons[x, y].gameObject.SetActive(false);
        fieldPolygons[x, y] = null;
    }

    private void LogPath(List<AstarNode> pathList)
    {
        string pathString = "";
        for (int k = 0; k < pathList.Count; k++)
        {
            pathString += "(" + pathList[k].GetNodeXValue() + ", " + pathList[k].GetNodeYValue() + ")" + "\t";
        }
        Debug.Log(pathString);
    }

    public Vector2 GetFieldPosition(int x, int y)
    {
        return fieldEachPosition[x, y];
    }
}
