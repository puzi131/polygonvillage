﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Database : MonoBehaviour
{
    public static Entity_EnemyWave enemyWave;
    public static Entity_Enemy enemy;
    public static Entity_Polygon polygon;
    public static Entity_Skill skill;
    public static Entity_PolygonSpecific polygonSpecific;

    // Start is called before the first frame update
    void Start()
    {
        enemyWave = TempLoadData.Instance.enemyWave;
        enemy = TempLoadData.Instance.enemy;
        polygon = TempLoadData.Instance.polygon;
        skill = TempLoadData.Instance.skill;
        polygonSpecific = TempLoadData.Instance.polygonSpecific;
    }

}
