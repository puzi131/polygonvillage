﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PrefabKind = PrefabManager.PrefabKind;
public class InstantiateManager : SingletonMonoBehaviour<InstantiateManager>
{
    private Dictionary<string, List<GameObject>> objectPool = new Dictionary<string, List<GameObject>>();

    /// <summary>
    /// Instance management using object pooling
    /// </summary>
    /// <param name="prefab"></param>
    /// <returns></returns>
    public GameObject Instantiate(PrefabKind kind, string prefab)
    {
        if (!objectPool.ContainsKey(prefab))
        {
            objectPool[prefab] = new List<GameObject>();
        }
        else
        {
            // Found idling object
            for (int i = 0; i < objectPool[prefab].Count; i++)
            {
                if (!objectPool[prefab][i].activeSelf)
                {
                    return objectPool[prefab][i];
                }
            }
        }
        GameObject choiceObject = Instantiate(PrefabManager.Instance.LoadPrefabSnyk(kind, prefab));
        objectPool[prefab].Add(choiceObject);
        choiceObject.name += objectPool[prefab].IndexOf(choiceObject);
        return choiceObject;
    }
}