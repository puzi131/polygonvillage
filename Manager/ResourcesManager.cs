﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourcesManager : SingletonMonoBehaviour<ResourcesManager>
{
    public enum ResourcesKind
    {
        prefab,
    }
    private Dictionary<ResourcesKind, Dictionary<string, Object>> resourcesMap = new Dictionary<ResourcesKind, Dictionary<string, Object>>() {
        {ResourcesKind.prefab, new Dictionary<string, Object>() }
    };
    private Dictionary<ResourcesKind, string> resourcesPathMap = new Dictionary<ResourcesKind, string>()
    {
        { ResourcesKind.prefab, "Prefabs/" }
    };

    public IEnumerator LoadResourcesAsynk(ResourcesKind kind, string name, System.Action<Object> callback)
    {
        if (resourcesMap[kind].ContainsKey(name))
        {
            callback(resourcesMap[kind][name]);
            yield break; 
        }
        string resourcesPath = resourcesPathMap[ResourcesKind.prefab] + name;
        ResourceRequest resourceRequest = Resources.LoadAsync(resourcesPath);
        while (!resourceRequest.isDone)
        {
            yield return null;
        }
        resourcesMap[kind].Add(name, resourceRequest.asset);
        callback(resourcesMap[kind][name]);
    }

    public Object LoadResourcesSynk(ResourcesKind kind, string name)
    {
        if (resourcesMap[kind].ContainsKey(name))
        {
            return resourcesMap[kind][name];
        }
        string resourcesPath = resourcesPathMap[ResourcesKind.prefab] + name;
        Object loadObject = Resources.Load(resourcesPath);
        resourcesMap[kind].Add(name, loadObject);
        return loadObject;
    }

}
