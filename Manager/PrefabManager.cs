﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ResourcesKind = ResourcesManager.ResourcesKind;

public class PrefabManager : SingletonMonoBehaviour<PrefabManager>
{
    public enum PrefabKind
    {
        Character,
        Bullet,
        Particle,
    }

    public Dictionary<PrefabKind, string> prefabPathMap = new Dictionary<PrefabKind, string>() {
        { PrefabKind.Character, "Characters/" },
        { PrefabKind.Bullet, "Bullets/" },
        {PrefabKind.Particle, "Particles/" },
    };

    private Dictionary<PrefabKind, Dictionary<string, GameObject>> prefabMap = new Dictionary<PrefabKind, Dictionary<string, GameObject>>()
    {
        { PrefabKind.Character, new Dictionary<string, GameObject>() },
        { PrefabKind.Bullet, new Dictionary<string, GameObject>() },
        { PrefabKind.Particle,new Dictionary<string, GameObject>() },
    };


    public GameObject LoadPrefabSnyk(PrefabKind prefabKind, string prefab)
    {
        if (prefabMap[prefabKind].ContainsKey(prefab))
        {
            return prefabMap[prefabKind][prefab];
        } else
        {
            string path = prefabPathMap[prefabKind] + prefab;
            GameObject loadGameObject = ResourcesManager.Instance.LoadResourcesSynk(ResourcesKind.prefab, path) as GameObject;
            prefabMap[prefabKind].Add(prefab, loadGameObject);
            return loadGameObject;
        }
    }
}
