﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Skill
{
    public class DamageObjectData
    {
        public enum DamageKind
        {
            Normal
        }

        public DamageKind damageKind;
        public AbnormalityObject abnormalityObject;
        public Enemy targetEnemy;
        public int damage;
        public float speed;
        public Vector2 basePosition;
    }

    public class AbnormalityObject
    {
        public enum Abnormality
        {
            None,
            Slow
        }
        public Abnormality abnormality;
        public string prefab;
        public GameObject abnormalityParticle;
        public float effect;
        public Timer timer;
    }
}
