﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Skill;

public class Polygon : Unit
{
    //public PolygonData data;
    public SkillBase skill;
    public ProductDrag productDrag;
    public delegate void UpdateState();
    private UpdateState updateState;

    public virtual void InitPolygon(int specificID)
    {
        Entity_Polygon.Param polygonData = Database.polygon.sheets[Common.BASE_SHEET].list.FirstOrDefault(data => data.specificID == specificID);
        prefabName = polygonData.prefab;
        name = polygonData.name;

        InitSkill(specificID);
    }

    protected virtual void InitSkill(int specificID)
    {
        Entity_PolygonSpecific.Param polygonSpecificData = Database.polygonSpecific.sheets[Common.BASE_SHEET].list.FirstOrDefault(data => data.id == specificID);
        switch(polygonSpecificData.skill)
        {
            case 1:
                skill = new SkillSlow();
                break;
            default:
                skill = new SkillNormal();
                break;
        }
        skill.Init(polygonSpecificData.skill);
        skill.SetSkillData(specificID);

    }

    public GameObject SetPrefab()
    {
        gameObject = InstantiateManager.Instance.Instantiate(PrefabManager.PrefabKind.Character, prefabName);
        transform = gameObject.transform;
        productDrag = gameObject.AddComponent<ProductDrag>();
        productDrag.InitProductDrag(this);

        updateState = Alert;

        return gameObject;
    }

    public void Update()
    {
        updateState?.Invoke();
        skill.Update();
    }

    public void Alert()
    {
    }

}
