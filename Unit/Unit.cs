﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit
{
    public string name;
    public string prefabName;
    public GameObject gameObject;
    public Transform transform;
}
