﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Skill;
using AstarNode = AStarAlgorithm.AstarNode;
using Abnormality = Skill.AbnormalityObject.Abnormality;

public class Enemy : Unit
{
    public EnemyData enemyData = new EnemyData();
    public delegate void UpdateBehavior();
    public UpdateBehavior updateBehavior;
    public bool isReady;    // 출격 준비
    private bool isDeath;


    public void InitEnemy()
    {
        enemyData.pathList = new List<AstarNode>();
        enemyData.abnormalities = new List<AbnormalityObject>();
        enemyData.pathIndex = 0;
        //enemyData.speed = 0.01f;
        //enemyData.startPositionX = 0;
        //enemyData.startPositionY = 6;
        //enemyData.hp = 23;
        isReady = false;
        isDeath = false;
        updateBehavior = null;
    }

    public void UpdateEnemy()
    {
        updateBehavior?.Invoke();
        UpdateAbnormality();
    }

    public void TowardDestination()
    {
        if (CheckDestination())
            Destroy();
        int x = enemyData.pathList[enemyData.pathIndex].GetNodeXValue();
        int y = enemyData.pathList[enemyData.pathIndex].GetNodeYValue();
        Vector2 destination = FieldManager.Instance.GetFieldPosition(x, y);
        if ((Vector2)transform.position == destination)
        {
            enemyData.pathIndex++;
        }
        MoveToPosition(destination);
    }

    public void MoveToPosition(Vector2 position)
    {
        float distance = Vector2.Distance(transform.position, position);
        Vector2 normaDirection = ((Vector3)position - transform.position).normalized;
        Vector3 calDestination = transform.position + (Vector3)normaDirection * enemyData.speed;
        float calDistance = Vector2.Distance(transform.position, calDestination);
        if (distance <= calDistance)
        {
            transform.position = position;
        }
        else
        {
            transform.position = calDestination;
        }
    }


    public void SetPath()
    {
        AStarAlgorithm aStarAlgorithm = new AStarAlgorithm();
        aStarAlgorithm.SetFieldInfo(FieldManager.Instance.CreateObstacle());
        enemyData.pathList = aStarAlgorithm.FindAStar(enemyData.startPositionX, enemyData.startPositionY);
    }

    public void SetPrefab()
    {
        isReady = true;
        gameObject = InstantiateManager.Instance.Instantiate(PrefabManager.PrefabKind.Character, prefabName);
        gameObject.SetActive(true);
        transform = gameObject.transform;
        transform.position = FieldManager.Instance.GetFieldPosition(enemyData.startPositionX, enemyData.startPositionY);
    }

    public void SetStartPosition(int x, int y)
    {
        enemyData.startPositionX = x;
        enemyData.startPositionY = y;
    }
    public void Departure()
    {
        updateBehavior = TowardDestination;
    }

    private bool CheckDestination()
    {
        if (enemyData == null)
        {
            Debug.LogError("Enemy data is null");
        }
        else if (enemyData.pathList == null)
        {
            Debug.LogError("Path list of enemy data is null");
        }
        if (enemyData.pathList.Count == enemyData.pathIndex + 1)
            return true;
        else
            return false;
    }

    private void Destroy()
    {
        EnemyManager.Instance.RemoveEnemy(this);
        isDeath = true;
    }

    public void UnderAttack(DamageObject damageObject)
    {
        enemyData.hp -= damageObject.data.damage;
        ProcessAbnormality(damageObject.data.abnormalityObject);

        if (enemyData.hp <= 0)
        {
            Destroy();
        }
    }

    public bool GetIsDeath()
    {
        return isDeath;
    }

    private void UpdateAbnormality()
    {
        for (int i = 0; i < enemyData.abnormalities.Count; i++)
        {
            switch (enemyData.abnormalities[i].abnormality)
            {
                case Abnormality.Slow:
                    UpdateSlow(enemyData.abnormalities[i]);
                    break;
            }
        }
    }

    private void UpdateSlow(AbnormalityObject abnormalityObject)
    {
        enemyData.speed = enemyData.fixedSpeed / abnormalityObject.effect;
        abnormalityObject.timer.UpdateTimer();

        if (abnormalityObject.timer.GetOverTime())
        {
            EndSlow(abnormalityObject);
        }
    }

    private void EndSlow(AbnormalityObject abnormalityObject)
    {
        abnormalityObject.abnormalityParticle.SetActive(false);
        enemyData.speed = enemyData.fixedSpeed;
        enemyData.abnormalities.Remove(abnormalityObject);
    }

    public void EndAbnormality()
    {
        for (int i = 0; i < enemyData.abnormalities.Count; i++)
        {
            enemyData.abnormalities[i].abnormalityParticle.SetActive(false);
        }
    }

    private void ProcessAbnormality(AbnormalityObject abnormalityObject)
    {
        // 데미지 오브젝트에 상태이상이 없을 경우 리턴
        if (abnormalityObject == null)
        {
            return;
        }

        for (int i = 0; i < enemyData.abnormalities.Count; i++)
        {
            // 원래 받고있는 상태이상이 더 강할경우 타이머 갱신
            if (enemyData.abnormalities[i].abnormality == abnormalityObject.abnormality &&
                enemyData.abnormalities[i].effect >= abnormalityObject.effect)
            {
                enemyData.abnormalities[i].timer.ResetTimer();
                return;
            }
        }

        // 받고있는 상태이상이 없을 경우 상태이상 추가
        ActiveAbnormality(abnormalityObject);
    }

    private void ActiveAbnormality(AbnormalityObject abnormalityObject)
    {
        enemyData.abnormalities.Add(abnormalityObject);
        GameObject particleObject = InstantiateManager.Instance.Instantiate(PrefabManager.PrefabKind.Particle, abnormalityObject.prefab);
        particleObject.transform.SetParent(transform);
        particleObject.transform.localPosition = Vector2.zero;
        particleObject.SetActive(true);
        abnormalityObject.abnormalityParticle = particleObject;
    }
}

