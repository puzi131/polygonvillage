﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Skill;
using AstarNode = AStarAlgorithm.AstarNode;
using Abnormality = Skill.AbnormalityObject.Abnormality;

public class EnemyData
{
    public int id;
    public int enemyNo;
    public List<AstarNode> pathList;
    public int pathIndex;
    public int startPositionX;
    public int startPositionY;
    // 기본 속도
    public float fixedSpeed;
    // 현재 가변 속도
    public float speed;
    public int maxHp;
    public int hp;
    public List<AbnormalityObject> abnormalities;
}