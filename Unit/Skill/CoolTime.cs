﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Skill
{
    public class CoolTime
    {
        private float fixedCoolTime;
        private float currentCoolTime;
        public bool IsAvailable { get; set; }

        public void Init()
        {
            IsAvailable = true;
            currentCoolTime = 0;
        }

        public void SetCoolTime(float coolTime)
        {
            fixedCoolTime = coolTime;
        }

        public void Update()
        {
            if (currentCoolTime > 0)
            {
                currentCoolTime -= Time.deltaTime;
                if (currentCoolTime <= 0)
                {
                    currentCoolTime = 0;
                    IsAvailable = true;
                }
            }
        }
        public void Active()
        {
            currentCoolTime = fixedCoolTime;
            IsAvailable = false;
        }
    }
}
