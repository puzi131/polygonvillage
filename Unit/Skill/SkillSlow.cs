﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Skill
{
    public class SkillSlow : SkillBase
    {
        public override void Init(int id)
        {
            base.Init(id);
        }

        public override void InspectionEnmey()
        {
            NormalInspectionEnemy();
        }

        public override void SearchTarget()
        {
            NormalSearchEnemy();
        }

        public override void AttemptAttack()
        {
            NormalAttemptAttack();
        }

        public override void Update()
        {
            base.Update();
            for (int i = 0; i < damageObjects.Count; i++)
            {
                damageObjects[i].MoveToEnemy();
            }
        }

        public override void LaunchBullet()
        {
            DamageObject newDamageObject = new DamageObject();
            newDamageObject.gameObject = InstantiateManager.Instance.Instantiate(PrefabManager.PrefabKind.Bullet, bulletPrefab);
            newDamageObject.gameObject.SetActive(true);
            newDamageObject.transform = newDamageObject.gameObject.transform;
            newDamageObject.data.targetEnemy = targetEnemy;

            newDamageObject.data.speed = bulletSpeed;
            newDamageObject.data.damageKind = DamageObjectData.DamageKind.Normal;

            newDamageObject.data.abnormalityObject = new AbnormalityObject();
            newDamageObject.data.abnormalityObject.abnormality = AbnormalityObject.Abnormality.Slow;
            newDamageObject.data.abnormalityObject.effect = abnormalityEffect;
            newDamageObject.data.abnormalityObject.prefab = abnormalityParticle;
            newDamageObject.data.abnormalityObject.timer = new Timer();
            newDamageObject.data.abnormalityObject.timer.SetFixedTime(abnormalityTime);

            newDamageObject.data.damage = attackPoint;

            newDamageObject.transform.position = basePosition;
            newDamageObject.SetCallBackRelease(ReleaseDamageObject);
            damageObjects.Add(newDamageObject);
        }

        public override void ReleaseDamageObject(DamageObject damageObject)
        {
            damageObjects.Remove(damageObject);
        }
    }
}
