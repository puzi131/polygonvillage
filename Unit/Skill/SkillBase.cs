﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Skill
{
    public abstract class SkillBase
    {
        public CoolTime coolTime = new CoolTime();
        protected delegate void TargetUpdate();
        protected TargetUpdate targetUpdate;

        public int id;
        public DamageObjectData.DamageKind damageKind;
        public List<DamageObject> damageObjects;
        public Enemy targetEnemy;
        public int maxTargetCount;
        public Vector2 basePosition;

        public int attackPoint;
        public float attackCoolTime;
        public float attackRange;

        protected float bulletSpeed;
        protected float abnormalityEffect;
        protected float abnormalityTime;
        protected string abnormalityParticle;
        protected string bulletPrefab;

        public virtual void Init(int id)
        {
            coolTime.Init();
            damageObjects = new List<DamageObject>();
            targetUpdate = SearchTarget;
            this.id = id;
            maxTargetCount = 1;

            Entity_Skill.Param skillData = Database.skill.sheets[Common.BASE_SHEET].list.FirstOrDefault(data => data.id == id);
            bulletSpeed = skillData.launchSpeed;
            abnormalityEffect = skillData.abnormalityEffect;
            abnormalityTime = skillData.abnormalityTime;
            abnormalityParticle = skillData.abnormalityParticle;
            bulletPrefab = skillData.prefab;
        }

        public virtual void Update()
        {
            coolTime.Update();
            targetUpdate?.Invoke();
        }

        public abstract void SearchTarget();
        public abstract void InspectionEnmey();
        public abstract void AttemptAttack();
        public virtual void LaunchBullet()
        {
            var skillInfo = Database.skill.sheets[Common.BASE_SHEET].list.FirstOrDefault(data => data.id == this.id);
            DamageObject newDamageObject = new DamageObject();
            newDamageObject.gameObject = InstantiateManager.Instance.Instantiate(PrefabManager.PrefabKind.Bullet, skillInfo.prefab);
            newDamageObject.gameObject.SetActive(true);
            newDamageObject.transform = newDamageObject.gameObject.transform;
            newDamageObject.data.targetEnemy = targetEnemy;
            newDamageObject.data.speed = skillInfo.launchSpeed;
            newDamageObject.data.damageKind = DamageObjectData.DamageKind.Normal;
            newDamageObject.data.abnormalityObject.abnormality = AbnormalityObject.Abnormality.Slow;
            newDamageObject.data.abnormalityObject.timer.SetFixedTime(1f);
            newDamageObject.data.damage = 1;
            newDamageObject.transform.position = basePosition;
            newDamageObject.SetCallBackRelease(ReleaseDamageObject);
            damageObjects.Add(newDamageObject);
        }
        public abstract void ReleaseDamageObject(DamageObject damageObject);

        public void SetSkillData(int specificID)
        {
            Entity_Polygon.Param polygonData = Database.polygon.sheets[Common.BASE_SHEET].list.FirstOrDefault(data => data.specificID == specificID);

            attackPoint = polygonData.damage;
            attackCoolTime = polygonData.attackCoolTime;
            attackRange = polygonData.range;
            coolTime.SetCoolTime(attackCoolTime);
        }

        protected void NormalSearchEnemy()
        {
            List<Enemy> enemyList = EnemyManager.Instance.GetActiveEnemyList();
            for (int i = 0; i < enemyList.Count; i++)
            {
                if (!enemyList[i].isReady)
                    continue;
                float distance = Vector2.Distance(enemyList[i].transform.position, basePosition);
                // 범위안에 들어온 적 검사
                if (distance < attackRange)
                {
                    targetEnemy = enemyList[i];
                    targetUpdate = NormalInspectionEnemy;
                    break;
                }
            }
        }

        protected void NormalInspectionEnemy()
        {
            float enemyDistance = Vector2.Distance(targetEnemy.transform.position, basePosition);
            if (attackRange < enemyDistance ||
                targetEnemy.GetIsDeath())
            {
                targetEnemy = null;
                targetUpdate = NormalSearchEnemy;
            } else
            {
                AttemptAttack();
            }
        }

        protected void NormalAttemptAttack()
        {
            if (targetEnemy == null)
                return;

            if (coolTime.IsAvailable)
            {
                LaunchBullet();
                coolTime.Active();
            }
        }

        public void SetBasePosition(Vector2 position)
        {
            basePosition = position;
        }
    }
}