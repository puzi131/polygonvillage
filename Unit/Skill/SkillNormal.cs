﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Skill
{
    public class SkillNormal : SkillBase
    {
        public override void Init(int id)
        {
            base.Init(id);
        }

        public override void Update()
        {
            base.Update();
            for (int i = 0; i < damageObjects.Count; i++)
            {
                damageObjects[i].MoveToEnemy();
            }
        }


        public override void AttemptAttack()
        {
            NormalAttemptAttack();
        }

        public override void LaunchBullet()
        {
            DamageObject newDamageObject = new DamageObject();
            newDamageObject.gameObject = InstantiateManager.Instance.Instantiate(PrefabManager.PrefabKind.Bullet, "8star");
            newDamageObject.gameObject.SetActive(true);
            newDamageObject.transform = newDamageObject.gameObject.transform;
            newDamageObject.data.targetEnemy = targetEnemy;
            newDamageObject.data.speed = 0.1f;
            newDamageObject.data.damageKind = DamageObjectData.DamageKind.Normal;
            newDamageObject.data.abnormalityObject = null;
            newDamageObject.data.damage = 1;
            newDamageObject.transform.position = basePosition;
            newDamageObject.SetCallBackRelease(ReleaseDamageObject);
            damageObjects.Add(newDamageObject);
        }

        public override void ReleaseDamageObject(DamageObject damageObject)
        {
            damageObjects.Remove(damageObject);
        }
        
        /// <summary>
        /// 타켓이 될 적 탐색
        /// </summary>
        public override void SearchTarget()
        {
            NormalSearchEnemy();
        }

        /// <summary>
        /// 타겟이 된 적 검사
        /// </summary>
        public override void InspectionEnmey()
        {
            NormalInspectionEnemy();
        }

    }
}