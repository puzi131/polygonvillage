﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Skill
{
    public class DamageObject
    {
        private Action<DamageObject> releasDamageObject;
        public DamageObjectData data = new DamageObjectData();
        public GameObject gameObject;
        public Transform transform;

        public void MoveToEnemy()
        {
            // 적이없으면 리턴
            if (!data.targetEnemy.gameObject.activeSelf)
            {
                ReleaseDamageObject();
                return;
            }
            // 차이
            Vector2 difference = data.targetEnemy.transform.position - transform.position;
            // 계산
            Vector2 calDifference = difference.normalized * data.speed;
            if (difference.magnitude < calDifference.magnitude)
            {
                // 도달함
                transform.position = data.targetEnemy.transform.position;
                data.targetEnemy.UnderAttack(this);
                ReleaseDamageObject();
            }
            else
            {
                // 도달하지 않음
                transform.position += (Vector3)calDifference;
            }
        }

        public void ReleaseDamageObject()
        {
            gameObject.SetActive(false);
            releasDamageObject?.Invoke(this);
        }

        public void SetCallBackRelease(Action<DamageObject> action)
        {
            releasDamageObject = action;
        }
    }
}