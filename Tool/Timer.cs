﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer
{
    private float fixedTime;
    private float time;

    public void SetFixedTime(float time)
    {
        fixedTime = time;
    }

    public float GetTime()
    {
        return time;
    }

    public void ResetTimer()
    {
        time = 0;
    }

    public void UpdateTimer()
    {
        time += Time.deltaTime;
    }

    public bool GetOverTime()
    {
        return fixedTime <= time;
    }

}
