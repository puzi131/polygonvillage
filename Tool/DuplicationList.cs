﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 중복 제거 리스트
/// </summary>
/// <typeparam name="T"></typeparam>
public class DuplicationList<T> : List<T>
{
    public new void Add(T item)
    {
        for (int i = 0; i < Count; i++)
        {
            if (this[i].Equals(item))
            {
                return;
            }
        }
        base.Add(item);
    }
}