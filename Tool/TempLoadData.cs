﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempLoadData : SingletonMonoBehaviour<TempLoadData>
{
    public Entity_EnemyWave enemyWave;
    public Entity_Enemy enemy;
    public Entity_Polygon polygon;
    public Entity_Skill skill;
    public Entity_PolygonSpecific polygonSpecific;
}
