﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DisplayFPS : MonoBehaviour
{
    private TextMeshProUGUI text;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
        StartCoroutine(CoroutineDisplayFPS());
    }

    private IEnumerator CoroutineDisplayFPS()
    {
        while(true)
        {
            float fps = 1 / Time.deltaTime;
            text.text = ((int)fps).ToString();
            yield return new WaitForSeconds(1);
        }
    }
}
